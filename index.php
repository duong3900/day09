<html>
<head>
<title></title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<?php 
     if ($_SERVER['REQUEST_METHOD'] == "POST") 
     {
        $_COOKIE = $_POST;
        $name = $_COOKIE['name'];
        $age = $_COOKIE['age'];
        $phone = $_COOKIE['phone'];
        $q1=  $_COOKIE['answer1'];
        $q2=  $_COOKIE['answer2'];
        $q3=  $_COOKIE['answer3'];
        $q4=  $_COOKIE['answer4'];
        $q5=  $_COOKIE['answer5'];
     
     
        if (isset($_POST['next'])) 
          {  

            if($_POST['name']) {
                setcookie( 'name', $name);
            }
            if($_POST['age']) {
                setcookie( 'age', $age);
            }
            if($_POST['phone']) {
                setcookie( 'phone', $phone);
            }
            if($_POST['answer1']) {
                setcookie( 'answer1', $q1);
            }

            if($_POST['answer2']) {
                setcookie( 'answer2', $q2);
            }

            if($_POST['answer3']) {
                setcookie( 'answer3', $q3);
            }

            if($_POST['answer4']) {
                setcookie( 'answer4', $q4);
            }

            if($_POST['answer5']) {
                setcookie( 'answer5', $q5);
            }
             header('location:index2.php');
          }
    }
    
    ?>

</head>
<body>
<div class="container">
<h2>Multiple Choice Questions Answers</h2>
<p>Please fill the details and answers the all questions-</p>
<form action="" method="post">
<div class="form-group">
<strong>Name*:</strong><br/>
 <input type="text" name="name" value="<?php echo isset($_COOKIE['name']) ? $_COOKIE['name'] : '';  ?>" required/>
</div>
<div class="form-group">
<strong>Age*:</strong><br/> 
<input type="text" name="age" value="<?php echo isset($_COOKIE['age']) ? $_COOKIE['age'] : '';  ?>" required/>
</div>
<div class="form-group">
<strong>Phone*:</strong><br/> 
<input type="text" name="phone" value="<?php echo isset($_COOKIE['phone']) ? $_COOKIE['phone'] : '';  ?>" required/>
</div>



<h3>Ques1 : Harry đã sử dụng câu thần chú nào để giết Chúa tể Voldemort? </h3>
<?php
        $question = array( "A" => "Expelliarmus", "B" => "Bảo trợ Expecto","C" => "Avada Kedavra","D" => "accio");
        foreach ($question as $i => $value)
              {
            echo
            "<br> </br>
            
            <input type='radio'  name='answer1' value='".$i. "'
            
            ";
            
            echo isset($_COOKIE['answer1']) && $_COOKIE['answer1'] == $i ? " checked" : "";
            echo "/>
                </label> ".$value;
            }
?>


<br/>



<div class="form-group"> 
<h3>Ques2 : Tại cuộc họp đầu tiên của Câu lạc bộ Dueling, Draco Malfoy đã triệu hồi con vật nào bằng câu thần chú 'Serpensortia'?</h3>
<?php
        $question = array( "A" => "con ếch", "B" => "con rắn","C" => "con rồng","D" => "con chó");
        foreach ($question as $i => $value)
              {
            echo
            "<br> </br>
            
            <input type='radio'  class='question' name='answer2' value='".$i. "'
            
            ";
            
            echo isset($_COOKIE['answer2']) && $_COOKIE['answer2'] == $i ? " checked" : "";
            echo "/>
                </label> ".$value;
            }
?>
<br/>
</div>


<div class="form-group"> 
<h3>Ques3 : Yếu tố nào được liên kết với Hufflepuff?</h3>
<?php
        $question = array( "A" => "Lửa", "B" => "Đất","C" => "Nước","D" => "Không khí");
        foreach ($question as $i => $value)
              {
            echo
            "<br> </br>
            
            <input type='radio'  class='question' name='answer3' value='".$i. "'
            
            ";
            
            echo isset($_COOKIE['answer3']) && $_COOKIE['answer3'] == $i ? " checked" : "";
            echo "/>
                </label> ".$value;
            }
?>
</div>


<div class="form-group">
<h3>Ques4 :  Tên của gia tinh nhà Black là gì?</h3>
<?php
        $question = array( "A" => "Dobby", "B" => "Nháy mắt","C" => "Kreacher","D" => "hockey");
        foreach ($question as $i => $value)
              {
            echo
            "<br> </br>
            
            <input type='radio'  class='question' name='answer4' value='".$i. "'
            
            ";
            
            echo isset($_COOKIE['answer4']) && $_COOKIE['answer4'] == $i ? " checked" : "";
            echo "/>
                </label> ".$value;
            }
?>
</div>

<div class="form-group">
<h3>Ques5 : Harry chơi ở vị trí nào trong đội Quidditch của mình?</h3>
<?php
        $question = array( "A" => "Người đi săn ", "B" => "Keeper","C" => "Seeker","D" => "Bludger");
        foreach ($question as $i => $value)
              {
            echo
            "<br> </br>
            
            <input type='radio'  class='question' name='answer5' value='".$i. "'
            
            ";
            
            echo isset($_COOKIE['answer5']) && $_COOKIE['answer5'] == $i ? " checked" : "";
            echo "/>
                </label> ".$value;
            }
?>
</div>


<div class="form-group">
<input type="submit" value="Next" name="next" class="btn btn-primary"/>
</div>
</form>
</div>
</body>
</html>