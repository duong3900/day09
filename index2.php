<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <title>Document</title>
    <?php 
     if ($_SERVER['REQUEST_METHOD'] == "POST") 
     {
        $_COOKIE = $_POST;
        
        $q6=  $_COOKIE['answer6'];
        $q7=  $_COOKIE['answer7'];
        $q8=  $_COOKIE['answer8'];
        $q9=  $_COOKIE['answer9'];
        $q10=  $_COOKIE['answer10'];
     
     
        if (isset($_POST['submit'])) 
          {  

            if($_POST['answer6']) {
                setcookie( 'answer6', $q6);
            }

            if($_POST['answer7']) {
                setcookie( 'answer7', $q7);
            }

            if($_POST['answer8']) {
                setcookie( 'answer8', $q8);
            }

            if($_POST['answer9']) {
                setcookie( 'answer9', $q9);
            }

            if($_POST['answer10']) {
                setcookie( 'answer10', $q10);
            }
             header('location: score.php');
          }
    }
    
    ?>
</head>
<body>
    <div class="container">
    <form action=" " method="post">
    
    
    <h3>Ques6 : Ai bảo vệ lối vào phòng sinh hoạt chung nhà Gryffindor? </h3>
    <div class="form-group"> 
    <?php
        $question = array( "A" => "Người phụ nữ màu xám", "B" => "Tu sĩ béo","C" => "Nam tước máu","D" => "Bà béo");
        foreach ($question as $i => $value)
              {
            echo
            "<br> </br>
            
            <input class='answer' type='radio'  class='question' name='answer6' value='".$i. "'
            
            ";
            
            echo isset($_COOKIE['answer6']) && $_COOKIE['answer6'] == $i ? " checked" : "";
            echo "/>
                </label> ".$value;
            }
    ?>
    </div>
    <br/>
    
    
    
    <div class="form-group"> 
    <h3>Ques7 : Ở giải đấu Triwizard , trong nhiệm vụ thứ hai Harry Potter đã làm gì để có thể thở dưới nước ?</h3>
    <?php
        $question = array( "A" => "Biến hình thành một con cá mập", "B" => "Hôn một nàng tiên cá","C" => 
        "Ăn một loại thảo dược có tên Cỏ Mang Cá","D" => "Thực hiện thần chú biến hình Transfiguration");
        foreach ($question as $i => $value)
              {
            echo
            "<br> </br>
            
            <input class='answer' type='radio'  class='question' name='answer7' value='".$i. "'
            
            ";
            
            echo isset($_COOKIE['answer7']) && $_COOKIE['answer7'] == $i ? " checked" : "";
            echo "/>
                </label> ".$value;
            }
    ?>
    </div>
    <br/>
    
    
    <div class="form-group"> 
    <h3>Ques8 : Lời nguyền nào KHÔNG phải là một trong những lời nguyền không thể tha thứ ?</h3>
    <?php
        $question = array( "A" => "Imperio", "B" => "Crucio","C" => 
        "Avada Kedavra","D" => "Sectumsempra");
        foreach ($question as $i => $value)
              {
            echo
            "<br> </br>
            
            <input class='answer' type='radio'  class='question' name='answer8' value='".$i. "'
            
            ";
            
            echo isset($_COOKIE['answer8']) && $_COOKIE['answer8'] == $i ? " checked" : "";
            echo "/>
                </label> ".$value;
            }
    ?>
    </div>
    
    
    <div class="form-group">
    <h3>Ques9 :  Tên sân ga đến trường Hogwatt?</h3>
    <?php
        $question = array( "A" => "6 ¾", "B" => "7 ¾","C" => 
        "8 ¾","D" => "9¾");
        foreach ($question as $i => $value)
              {
            echo
            "<br> </br>
            
            <input class='answer' type='radio'  class='question' name='answer9' value='".$i. "'
            
            ";
            
            echo isset($_COOKIE['answer9']) && $_COOKIE['answer9'] == $i ? " checked" : "";
            echo "/>
                </label> ".$value;
            }
    ?>
    </div>
    
    <div class="form-group">
    <h3>Ques10 : Ai đã cho Harry Potter chiếc áo tàng hình ?</h3>
    <?php
        $question = array( "A" => "Thầy Dumbledore", "B" => "Mắt điên","C" => 
        "James Potter","D" => "Sirius Black");
        foreach ($question as $i => $value)
              {
            echo
            "<br> </br>
            
            <input class='answer' type='radio'  class='question' name='answer10' value='".$i. "'
            
            ";
            
            echo isset($_COOKIE['answer10']) && $_COOKIE['answer10'] == $i ? " checked" : "";
            echo "/>
                </label> ".$value;
            }
    ?>
    </div>
    
    
    <div class="form-group">
    <input type="submit" value="Nộp bài" name="submit" class="btn btn-primary"/>
    </div>
    </form>
    </div>
    </body>
</html>